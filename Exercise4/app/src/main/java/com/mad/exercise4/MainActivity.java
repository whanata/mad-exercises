package com.mad.exercise4;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

/**
 * A simple class to show jokes from `http://www-staff.it.uts.edu.au/~rheise/sarcastic.cgi`
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity {
    public static final String LOG_TAG = "MainActivity";
    public static final String JOKES_URL = "http://www-staff.it.uts.edu.au/~rheise/sarcastic.cgi?len=%s";

    private Spinner mJokeLengthSpinner;
    private TextView mJokeTextView;
    private Button mOneJokeButton;
    private Button mThreeJokeButton;

    private String jokeLength;

    /**
     * Initialise variables - called during app start
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mJokeLengthSpinner = Helper.getSpinner(this, R.id.activity_main_joke_length_spinner);
        this.mJokeTextView = Helper.getTextView(this, R.id.activity_main_joke_textview);
        this.mOneJokeButton = Helper.getButton(this, R.id.activity_main_one_joke_button);
        this.mThreeJokeButton = Helper.getButton(this, R.id.activity_main_three_joke_button);

        this.mJokeLengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                jokeLength = mJokeLengthSpinner.getItemAtPosition(i).toString();
                Toast.makeText(
                        MainActivity.this,
                        String.format(getString(R.string.joke_length_selected), jokeLength),
                        Toast.LENGTH_SHORT
                ).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        this.addOnClickListener();
    }

    /**
     * Initialise all onClickListeners
     */
    protected void addOnClickListener() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(view.getId()) {
                    case R.id.activity_main_one_joke_button:
                        oneJokeAction();
                        break;
                    case R.id.activity_main_three_joke_button:
                        threeJokesAction();
                        break;
                    default:
                        break;
                }
            }
        };

        this.mOneJokeButton.setOnClickListener(listener);
        this.mThreeJokeButton.setOnClickListener(listener);
    }

    /**
     * Execute action to get one joke
     */
    protected void oneJokeAction() {
        new getJoke().execute();
    }

    /**
     * Execute action to get three jokes
     */
    protected void threeJokesAction() {
        new getJokes(3).execute();
    }

    /**
     * AsyncTask for getting and displaying one joke
     */
    private class getJoke extends AsyncTask<Void, Void, String> {
        private ProgressDialog mProgressDialog;

        /**
         * Cancel - Close mProgressDialog and show string that download failed
         */
        @Override
        protected void onCancelled() {
            super.onCancelled();
            mJokeTextView.setText(getString(R.string.fail_download_joke));

            this.closeProgressDialog();
        }

        /**
         * Get joke from MainActivity.JOKES_URL and return it
         * @param voids
         * @return The joke from MainActivity.JOKES_URL
         */
        @Override
        protected String doInBackground(Void... voids) {
            String joke = "";
            try {
                // URL includes joke length wanted
                URL url = new URL(String.format(MainActivity.JOKES_URL, jokeLength));
                // Open URL connection
                URLConnection conn = url.openConnection();
                // Obtain the input stream
                BufferedReader in =
                        new BufferedReader(new InputStreamReader(conn.getInputStream()));
                // Get joke (only in 1 line)
                joke = in.readLine();
                // Close the connection
                in.close();
            } catch (IOException e) {
                // Close mProgressDialog when fail to download joke
                cancel(true);
            }
            return joke;
        }

        /**
         * Create progress spinner for one joke
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(MainActivity.LOG_TAG, "Start Background Thread");
            this.mProgressDialog = new ProgressDialog(MainActivity.this);
            this.mProgressDialog.setIndeterminate(false);
            this.mProgressDialog.setMessage("Downloading joke...");
            this.mProgressDialog.show();
        }

        /**
         * Remove progress spinner
         * @param joke The joke returned by this.doInBackground()
         */
        @Override
        protected void onPostExecute(String joke) {
            super.onPostExecute(joke);
            Log.d(MainActivity.LOG_TAG, "Set Joke: " + joke);
            mJokeTextView.setText(joke);

            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }

        protected void closeProgressDialog() {
            if (this.mProgressDialog.isShowing()) {
                this.mProgressDialog.dismiss();
            }
        }
    }

    /**
     * AsyncTask for getting and displaying multiple jokes
     */
    private class getJokes extends AsyncTask<Void, Integer, String[]> {
        private int numJokes;
        private ProgressDialog mProgressDialog;

        /**
         * Constructor - Set number of jokes grabbed from MainActivity.JOKES_URL
         * @param numJokes
         */
        public getJokes(int numJokes) {
            this.numJokes = numJokes;
        }

        /**
         * Cancel - Close mProgressDialog and show string that download failed
         */
        @Override
        protected void onCancelled() {
            super.onCancelled();
            mJokeTextView.setText(getString(R.string.fail_download_joke));

            this.closeProgressDialog();
        }

        /**
         * Get multiple joke from MainActivity.JOKES_URL and return it
         * @param voids
         * @return The joke array from MainActivity.JOKES_URL
         */
        @Override
        protected String[] doInBackground(Void... voids) {
            String[] jokes = new String[this.numJokes];
            try {
                // URL includes current joke length
                URL url = new URL(String.format(MainActivity.JOKES_URL, jokeLength));
                // Get number of jokes specified by this.numJokes
                for (int i = 0; i < this.numJokes; i++) {
                    // Open a connection to the web service
                    URLConnection conn = url.openConnection();
                    // Obtain the input stream
                    BufferedReader in =
                            new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    // Get joke (only in 1 line)
                    String joke = in.readLine();
                    jokes[i] = joke;
                    publishProgress(i);
                    // Close the connection
                    in.close();
                }
            } catch (IOException e) {
                // Close mProgressDialog when fail to download joke
                cancel(true);
            }
            return jokes;
        }

        /**
         * Initialise the Progress Bar for downloading multiple jokes
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(MainActivity.LOG_TAG, "Start Background Thread");
            this.mProgressDialog = new ProgressDialog(MainActivity.this);
            this.mProgressDialog.setIndeterminate(false);
            this.mProgressDialog.setMax(this.numJokes);
            this.mProgressDialog.setMessage(String.format(getString(R.string.downloading_joke), 1));
            this.mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.mProgressDialog.show();
        }

        /**
         * Display jokes
         * @param jokeList jokes array from this.doInBackground()
         */
        @Override
        protected void onPostExecute(String[] jokeList) {
            super.onPostExecute(jokeList);
            String jokeString = "";

            for (String joke: jokeList) {
                jokeString += joke + "\n\n";
            }

            mJokeTextView.setText(jokeString.trim());

            this.closeProgressDialog();
        }

        /**
         * Update progress bar (ProgressDialog) for every new joke downloaded
         * @param progress Number of jokes downloaded
         */
        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            this.mProgressDialog.incrementProgressBy(1);

            int jokeNum = progress[0] + 1;
            int jokeNumDownloading = jokeNum + 1;

            if (jokeNumDownloading > 3) {
                jokeNumDownloading = 3;
            }
            this.mProgressDialog.setMessage(
                    String.format(getString(R.string.downloading_joke), jokeNumDownloading)
            );
        }

        /**
         * Close ProgressDialog
         */
        protected void closeProgressDialog() {
            if (this.mProgressDialog.isShowing()) {
                this.mProgressDialog.dismiss();
            }
        }
    }
}
