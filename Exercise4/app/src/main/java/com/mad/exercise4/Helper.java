package com.mad.exercise4;

import android.app.Activity;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * A helper class for this exercise, mainly used only for getting widgets
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class Helper {

    /**
     * Get button from activity
     * @param activity Current activity class
     * @param id ID of Button
     * @return Button object
     */
    public static Button getButton(Activity activity, int id) {
        return (Button)activity.findViewById(id);
    }

    /**
     * Get TextView from activity
     * @param activity Current activity class
     * @param id ID of TextView
     * @return TextView object
     */
    public static TextView getTextView(Activity activity, int id) {
        return (TextView)activity.findViewById(id);
    }

    /**
     * Get Spinner from activity
     * @param activity Current activity class
     * @param id ID of Spinner
     * @return Spinner object
     */
    public static Spinner getSpinner(Activity activity, int id) {
        return (Spinner)activity.findViewById(id);
    }
}
