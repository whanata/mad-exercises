package com.mad.exercise1;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button submitBtn = (Button) findViewById(R.id.submit_button);
        View parentLayout = findViewById(android.R.id.content);
        Snackbar.make(parentLayout, getString(R.string.welcome) + "!", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText firstNameText = (EditText) findViewById(R.id.first_name);
                String firstName = firstNameText.getText().toString();
                EditText lastNameText = (EditText) findViewById(R.id.last_name);
                String lastName = lastNameText.getText().toString();
                String helloString = String.format("%s %s %s", getString(R.string.hello), firstName, lastName).trim();
                helloString += "!";
                Snackbar.make(view, helloString, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
