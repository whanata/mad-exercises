package com.mad.exercise6.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.mad.exercise6.model.Train;

/**
 * All DB Functions must be stored here. It stores all DB operations including
 * initialising DB, Adding and Deleting rows on the table
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    public static final String LOG_TAG = "DatabaseHelper";

    private static DatabaseHelper sInstance;

    private static final String DATABASE_NAME = "main";
    private static final int DATABASE_VERSION = 1;

    // Trains Table
    private static final String TABLE_TRAINS = "trains";
    private static final String KEY_TRAINS_ID = "id";
    private static final String KEY_TRAINS_PLATFORM = "platform";
    private static final String KEY_TRAINS_ARRIVAL_TIME = "arrival_time";
    private static final String KEY_TRAINS_STATUS = "status";
    private static final String KEY_TRAINS_DESTINATION = "destination";
    private static final String KEY_TRAINS_DESTINATION_TIME = "destination_time";

    /**
     * Create a singleton instance of DatabaseHelper. If DatabaseHelper object already exist,
     * return existing instance of DatabaseHelper
     * @param context The current environment
     * @return DatabaseHelper instance
     */
    public static synchronized DatabaseHelper getInstance(Context context) {
        if (DatabaseHelper.sInstance == null) {
            DatabaseHelper.sInstance = new DatabaseHelper(context.getApplicationContext());
        }
        return DatabaseHelper.sInstance;
    }

    /**
     * Constructor should be private to prevent direct instantiation.
     * make call to static method "getInstance()" instead.
     * @param context The current environment
     */
    private DatabaseHelper(Context context) {
        super(context, DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.DATABASE_VERSION);
    }

    /**
     * Create trains table. This function runs during a creation of a new DatabaseHelper instance.
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTrainsTable = String.format("CREATE TABLE %s", DatabaseHelper.TABLE_TRAINS) +
                "(" +
                    String.format("%s INTEGER PRIMARY KEY,", DatabaseHelper.KEY_TRAINS_ID) +
                    String.format("%s TEXT,", DatabaseHelper.KEY_TRAINS_PLATFORM) +
                    String.format("%s INTEGER,", DatabaseHelper.KEY_TRAINS_ARRIVAL_TIME) +
                    String.format("%s TEXT,", DatabaseHelper.KEY_TRAINS_STATUS) +
                    String.format("%s TEXT,", DatabaseHelper.KEY_TRAINS_DESTINATION) +
                    String.format("%s TEXT", DatabaseHelper.KEY_TRAINS_DESTINATION_TIME) +
                ")";


        db.execSQL(createTrainsTable);
    }

    /**
     * Drops all table and create new table from onCreate function if database version is different
     * from the previous instance
     * @param db The DB Instance
     * @param oldVersion The old database version
     * @param newVersion The new database version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Called during upgrade, new database version
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseHelper.TABLE_TRAINS);
            onCreate(db);
        }
    }

    /**
     * Insert train in the trains table.
     * @param train The new train object as a new row in the table.
     */
    public void addTrain(Train train) {
        SQLiteDatabase db = getWritableDatabase();

        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(DatabaseHelper.KEY_TRAINS_PLATFORM, train.getPlatform​());
            values.put(DatabaseHelper.KEY_TRAINS_ARRIVAL_TIME, train.getArrivalTime​());
            values.put(DatabaseHelper.KEY_TRAINS_STATUS, train.getStatus());
            values.put(DatabaseHelper.KEY_TRAINS_DESTINATION, train.getDestination​());
            values.put(DatabaseHelper.KEY_TRAINS_DESTINATION_TIME, train.getDestinationTime​());

            db.insertOrThrow(DatabaseHelper.TABLE_TRAINS, null, values);
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.d(DatabaseHelper.LOG_TAG, "Error while inserting into DB", e);
        } finally {
            db.endTransaction();
        }
    }

    /**
     * Truncate trains table
     */
    public void deleteAllTrain() {
        SQLiteDatabase db = getWritableDatabase();

        db.beginTransaction();
        try {
            db.execSQL("DELETE FROM " + DatabaseHelper.TABLE_TRAINS);
            db.execSQL("VACUUM");
            db.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.d(DatabaseHelper.LOG_TAG, "Error while truncating table", e);
        } finally {
            db.endTransaction();
        }
    }
}
