package com.mad.exercise6.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mad.exercise6.R;
import com.mad.exercise6.adapter.TrainAdapter;
import com.mad.exercise6.db.DatabaseHelper;
import com.mad.exercise6.model.Train;

import java.util.ArrayList;
import java.util.Random;

/**
 * The Main Activity for this application. Shows list of train information in a RecyclerView.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity {
    public static final String LOG_TAG = "MainActivity";
    public static final String PLATFORM_KEY = "PLATFORM";
    public static final String ARRIVAL_TIME_KEY = "ARRIVAL_TIME";
    public static final String STATUS_KEY = "STATUS";
    public static final String DESTINATION_KEY = "DESTINATION";
    public static final String DESTINATION_TIME_KEY = "DESTINATION_TIME";
    public static final int ADD_TRAIN_REQUEST_CODE = 1;

    private ArrayList<Train> mTrainList;
    private RecyclerView mRecyclerView;
    private ProgressBar mMainProgressBar;
    private TrainAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FloatingActionButton mAddTrainButton;

    /**
     * Function always runs when app is initialized.
     * Initialise on the member variables.
     * Sets up the RecyclerView of the trains.
     * Add dummy information of the trains.
     * Add onClickListener.
     * @param savedInstanceState The information saved during a previous instance.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.mTrainList =  new ArrayList<>();
        this.mMainProgressBar = (ProgressBar) findViewById(R.id.content_main_main_ProgressBar);
        this.mRecyclerView = (RecyclerView) findViewById(R.id.content_main_train_RecyclerView);
        this.mAdapter = new TrainAdapter(this, this.mTrainList);
        this.mLayoutManager = new LinearLayoutManager(getApplicationContext());
        this.mAddTrainButton = (FloatingActionButton)
                findViewById(R.id.activity_main_add_train_FloatingActionButton);

        // Setup Recycler View for Trains
        this.mRecyclerView.setLayoutManager(this.mLayoutManager);
        this.mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        this.mRecyclerView.setAdapter(mAdapter);

        this.prepareTrainData();

        this.addOnClickListener();
    }

    /**
     * Private Inner Class for refreshing all the train arrival time in a background thread.
     */
    private class refreshAll extends AsyncTask<Void, Void, Void> {

        /**
         * For each train, get a new arrival time between 1 and 20 inclusively.
         * Sleep for 3 seconds after this.
         * @param voids
         * @return null
         */
        @Override
        protected Void doInBackground(Void... voids) {
            for (Train train: mTrainList) {
                int arrivalTime = this.getRandomInt(1, 20);
                train.setArrivalTime​(arrivalTime);
            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                Log.d(MainActivity.LOG_TAG, "Interrupt Sleeping", e);
            }
            return null;
        }

        /**
         * Remove RecyclerView and replace it with a ProgressBar (Spinner).
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mRecyclerView.setVisibility(View.GONE);
            mMainProgressBar.setVisibility(View.VISIBLE);
        }

        /**
         * Notify TrainAdapter (RecyclerView) to update all the data.
         * Also remove the ProgressBar and replace it back with the RecyclerView.
         * @param empty
         */
        @Override
        protected void onPostExecute(Void empty) {
            super.onPostExecute(empty);
            mAdapter.notifyDataSetChanged();
            mMainProgressBar.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }

        /**
         * Get a random int from a range between `min` and `max` inclusively.
         * @param min The lowest number the random integer can be.
         * @param max The highest number the random integer can be.
         * @return A random integer between `min` and `max` inclusively.
         */
        protected int getRandomInt(int min, int max) {
            Random random = new Random();
            return random.nextInt((max - min) + 1) + min;
        }
    }

    /**
     * Create the menu, inflate the menu when clicked.
     * @param menu
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Initialising the listener for selecting items in the menu.
     * @param item The MenuItem object that was selected.
     * @return True
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                refresh_all_action();
                break;
            case R.id.action_delete_all:
                delete_all_action();
                break;
            case R.id.action_quit:
                finish();
                break;
        }
        return true;
    }

    /**
     * Add onClickListener
     */
    protected void addOnClickListener() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(view.getId()) {
                    case R.id.activity_main_add_train_FloatingActionButton:
                        addTrainAction();
                        break;
                    default:
                        break;
                }
            }
        };

        this.mAddTrainButton.setOnClickListener(listener);
    }

    /**
     * Send intent to AddTrainActivity which will add the trains.
     */
    protected void addTrainAction() {
        Intent addTrainIntent = new Intent(this, AddTrainActivity.class);
        startActivityForResult(addTrainIntent, MainActivity.ADD_TRAIN_REQUEST_CODE);
    }

    /**
     * Remove all of the rows in the RecyclerView.
     * Also truncate the Trains Table.
     */
    protected void delete_all_action() {
        this.mTrainList.clear();
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(this);
        databaseHelper.deleteAllTrain();
        this.mAdapter.notifyDataSetChanged();
    }

    /**
     * The action of refreshing all the arrival time.
     */
    protected void refresh_all_action() {
        new refreshAll().execute();
    }

    /**
     * Get intent from AddTrainActivity and add the trains from the information in the Intent.
     * @param requestCode The request code from the intent
     * @param resultCode The result code (If successful or not).
     * @param data The intent data from the another activity that sent the intent.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        View view = findViewById(R.id.content_main_root);
        switch (requestCode) {
            case MainActivity.ADD_TRAIN_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    String trainAddedMessage = getString(R.string.add_train_message);
                    Toast.makeText(getApplicationContext(), trainAddedMessage, Toast.LENGTH_SHORT).show();

                    String platform = data.getStringExtra(MainActivity.PLATFORM_KEY);
                    int arrivalTime = Integer.parseInt(data.getStringExtra(MainActivity.ARRIVAL_TIME_KEY));
                    String status = data.getStringExtra(MainActivity.STATUS_KEY);
                    String destination = data.getStringExtra(MainActivity.DESTINATION_KEY);
                    String destinationTime = data.getStringExtra(MainActivity.DESTINATION_TIME_KEY);

                    addTrain(platform, arrivalTime, status, destination, destinationTime);
                    this.mAdapter.notifyItemInserted(this.mTrainList.size() - 1);
                }
                break;
            default:
                break;
        }
    }

    /**
     * Adding new train object in the mTrainList.
     * Adding that new train row to the Trains table.
     * @param platform​
     * @param arrivalTime​
     * @param status
     * @param destination​
     * @param destinationTime
     */
    protected void addTrain(String platform,
                            int arrivalTime,
                            String status,
                            String destination,
                            String destinationTime) {
        Train train;
        train = new Train(platform, arrivalTime, status, destination, destinationTime);
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(this);
        databaseHelper.addTrain(train);
        this.mTrainList.add(train);
    }

    /**
     * Prepare dummy data in mTrainList.
     * Also notify the change in mTrainList for the RecyclerView so it is refresh with the new
     * information.
     */
    private void prepareTrainData() {
        this.addTrain("Hurstville Platform 5", 10, "On time", "Allawah", "14:20");

        this.addTrain("Allawah Platform 5", 5, "On time", "Penshurst", "15:25");

        this.addTrain("Wolli Creek Platform 23", 12, "Late", "Penshurst", "15:55");

        this.addTrain("Arncliffe Platform 7", 30, "On time", "Sutherland", "05:55");

        this.addTrain("Punchbowl Platform 1", 1, "Late", "Lakemba", "02:54");

        this.addTrain("Beverly Hills Platform 6", 1, "On time", "Punchbowl", "10:45");

        this.addTrain("Westmead Platform 2", 3, "Late", "Paramatta", "14:55");

        this.addTrain("North Sydney Platform 16", 8, "On time", "Chatswood", "18:35");

        this.addTrain("Mortdale Platform 5", 16, "Late", "Waterfall", "17:50");

        this.addTrain("Bondi Junction Platform 8", 13, "On time", "Martin Place", "19:30");

        this.mAdapter.notifyDataSetChanged();
    }
}
