package com.mad.exercise6.model;

/**
 * A class that stores information about a specific train arrival and departure
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class Train {
    private String mPlatform​;
    private int mArrivalTime​;
    private String mStatus;
    private String mDestination​;
    private String mDestinationTime​;

    /**
     * Constructor of the Train Class. Initialises the member variables.
     * @param platform​ Station and platform the train will be arriving to.
     * @param arrivalTime​ How long it takes before it arrives at the station.
     * @param status The status of the train coming (Late or On Time).
     * @param destination​ The destination of the train.
     * @param destinationTime​ The time when the train reaches its destination.
     */
    public Train(String platform,
                 int arrivalTime,
                 String status,
                 String destination,
                 String destinationTime) {
        this.mPlatform​ = platform;
        this.mArrivalTime​ = arrivalTime;
        this.mStatus = status;
        this.mDestination​ = destination;
        this.mDestinationTime​ = destinationTime;
    }

    /**
     * Get Platform
     * @return Platform of Train
     */
    public String getPlatform​() {
        return this.mPlatform​;
    }

    /**
     * Set Platform
     * @param platform​ The platform of arriving train
     */
    public void setPlatform​(String platform) {
        this.mPlatform​ = platform;
    }

    /**
     * Get Arrival Time
     * @return Arrival Time of Train
     */
    public int getArrivalTime​() {
        return this.mArrivalTime​;
    }

    /**
     * Set Arrival Time
     * @param arrivalTime​ Arrival Time of train
     */
    public void setArrivalTime​(int arrivalTime) {
        this.mArrivalTime​ = arrivalTime;
    }


    /**
     * Get status of train arriving
     * @return Status of train arriving
     */
    public String getStatus() {
        return this.mStatus;
    }

    /**
     * Set status of train arriving
     * @param status status of train arriving
     */
    public void setStatus(String status) {
        this.mStatus = status;
    }

    /**
     * Get destination of the train
     * @return Destination of the train
     */
    public String getDestination​() {
        return this.mDestination​;
    }

    /**
     * Set destination of the train
     * @param destination
     */
    public void setDestination(String destination) {
        this.mDestination​ = destination;
    }

    /**
     * Get Destination Time of train
     * @return Destination Time of train
     */
    public String getDestinationTime​() {
        return this.mDestinationTime​;
    }

    /**
     * Set Destination Time of train
     * @param destinationTime​ Destination Time of train
     */
    public void setDestinationTime​(String destinationTime) {
        this.mDestinationTime​ = destinationTime;
    }

    /**
     * Check if train is late by looking at status of train
     * @return Train is late or not
     */
    public boolean isLate() {
        return this.mStatus.equals("Late");
    }
}
