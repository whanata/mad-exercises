package com.mad.exercise6.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.mad.exercise6.R;

/**
 * Adding Train Activity, governs the activity for adding new trains.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class AddTrainActivity extends AppCompatActivity {
    private EditText mPlatformEditText;
    private EditText mArrivalTimeEditText;
    private Spinner mStatusSpinner;
    private EditText mDestinationEditText;
    private EditText mDestinationTimeEditText;
    private Button mAddButton;
    private Button mCancelButton;

    /**
     * This is run during the creation of this activity.
     * Set the title to `Add Train`.
     * Initialise member variable.
     * Add onClickListener.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_train);
        getSupportActionBar().setTitle("Add Train");

        this.getAllView();

        this.addOnClickListener();
    }

    /**
     * Initialise all the views in the member variable.
     */
    protected void getAllView() {
        mPlatformEditText = (EditText) findViewById(R.id.activity_add_train_platform_EditText);
        mArrivalTimeEditText = (EditText) findViewById(R.id.activity_add_train_arrival_time_EditText);
        mStatusSpinner = (Spinner) findViewById(R.id.activity_add_train_status_Spinner);
        mDestinationEditText = (EditText) findViewById(R.id.activity_add_train_destination_EditText);
        mDestinationTimeEditText = (EditText) findViewById(R.id.activity_add_train_destination_time_EditText);
        mAddButton = (Button) findViewById(R.id.activity_add_train_add_Button);
        mCancelButton = (Button) findViewById(R.id.activity_add_train_cancel_Button);
    }

    /**
     * Add onClickListener (Adding and Cancelling button)
     */
    protected void addOnClickListener() {
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(view.getId()) {
                    case R.id.activity_add_train_add_Button:
                        addAction();
                        break;
                    case R.id.activity_add_train_cancel_Button:
                        cancelAction();
                        break;
                    default:
                        break;
                }
            }
        };

        this.mAddButton.setOnClickListener(listener);
        this.mCancelButton.setOnClickListener(listener);
    }

    /**
     * Action to adding new activity, will sent information through intent to MainActivity.
     * The information comes from the EditText and Spinner member variables.
     */
    protected void addAction() {
        Intent trainIntent = new Intent();

        String platform = this.mPlatformEditText.getText().toString();
        String arrivalTime = this.mArrivalTimeEditText.getText().toString();
        String status = this.mStatusSpinner.getSelectedItem().toString();
        String destination = this.mDestinationEditText.getText().toString();
        String destinationTime = this.mDestinationTimeEditText.getText().toString();

        trainIntent.putExtra(MainActivity.PLATFORM_KEY, platform);
        trainIntent.putExtra(MainActivity.ARRIVAL_TIME_KEY, arrivalTime);
        trainIntent.putExtra(MainActivity.STATUS_KEY, status);
        trainIntent.putExtra(MainActivity.DESTINATION_KEY, destination);
        trainIntent.putExtra(MainActivity.DESTINATION_TIME_KEY, destinationTime);

        setResult(Activity.RESULT_OK, trainIntent);
        finish();
    }

    /**
     * Close activity - goes to MainActivity.
     */
    protected void cancelAction() {
        finish();
    }
}
