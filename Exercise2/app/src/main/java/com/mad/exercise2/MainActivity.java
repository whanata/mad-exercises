package com.mad.exercise2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

/**
 * MainActivity.java - A simple class to get user information by inputs from the screen.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String NAME_KEY = "NAME";
    public static final String EMAIL_KEY = "EMAIL";
    public static final String PHONE_KEY = "PHONE";
    public static final String PHONE_TYPE_KEY = "PHONE_TYPE";
    public static final String AGREE_KEY = "AGREED_STRING";
    public static final int REQUEST_CODE = 1;

    private EditText nameEditText;
    private EditText emailEditText;
    private EditText phoneEditText;
    private Spinner phoneSpinner;
    private Button submitButton;
    private Button clearButton;
    private Button exitButton;

    /**
     * Use to instantiate my variables
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MainActivity", "onCreate()");
        setContentView(R.layout.activity_main);

        this.nameEditText = this.getEditText(R.id.activity_main_name_edittext);
        this.emailEditText = this.getEditText(R.id.activity_main_email_edittext);
        this.phoneEditText = this.getEditText(R.id.activity_main_phone_edittext);
        this.phoneSpinner = (Spinner) findViewById(R.id.activity_main_phone_type_spinner);
        this.submitButton = this.getButton(R.id.activity_main_submit_button);
        this.clearButton = this.getButton(R.id.activity_main_clear_all_button);
        this.exitButton = this.getButton(R.id.activity_main_exit_button);

        this.submitButton.setOnClickListener(this);
        this.clearButton.setOnClickListener(this);
        this.exitButton.setOnClickListener(this);
    }

    /**
     * onStart - Android Lifecyle
     */
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity", "onStart()");
    }

    /**
     * onRestart - Android Lifecycle
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MainActivity", "onRestart()");
    }

    /**
     * onResume - Android Lifecycle
     */
    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity", "onResume()");
    }

    /**
     * onPause - Android Lifecycle
     */
    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity", "onPause()");
    }

    /**
     * onStop - Android Lifecycle
     */
    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop()");
    }

    /**
     * onDestroy - Android Lifecycle
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity", "onDestroy()");
    }

    /**
     * Does something when a widget has been clicked
     * @param view Current View
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_main_submit_button:
                this.submit_action(view);
                break;
            case R.id.activity_main_clear_all_button:
                this.clear_all_action();
                break;
            case R.id.activity_main_exit_button:
                this.exit_action();
                break;
            default:
                break;
        }
    }

    /**
     * Display if user has agree or disagree with the information from ActivityTwo
     * @param requestCode The request code used for the exchange
     * @param resultCode The result code - if successful or not etc.
     * @param data The Intent Object that was sent back
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        View view = findViewById(R.id.activity_main_root);
        switch (requestCode) {
            case MainActivity.REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    String snackBarString = data.getStringExtra(MainActivity.AGREE_KEY);
                    Snackbar.make(view, snackBarString, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                break;
            default:
                break;
        }
    }

    /**
     * Get Button from layout
     * @param id ID of Button
     * @return Button Object with that specified ID
     */
    protected Button getButton(int id) {
        return (Button)findViewById(id);
    }

    /**
     * Get EditText from layout
     * @param id ID of EditText
     * @return EditText Object with that specified ID
     */
    protected EditText getEditText(int id) {
        return (EditText)findViewById(id);
    }

    /**
     * Show Snackbar saying submit button has been clicked
     * Get all values from the input and send it to ActivityTwo (StartActivityForResult)
     * @param view Current View
     */
    protected void submit_action(View view) {
        Intent submitIntent = new Intent(this, ActivityTwo.class);

        Snackbar.make(view, getString(R.string.submit_button_clicked), Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();

        String name = this.nameEditText.getText().toString();
        String email = this.emailEditText.getText().toString();
        String phone = this.phoneEditText.getText().toString();
        String phoneType = this.phoneSpinner.getSelectedItem().toString();

        submitIntent.putExtra(MainActivity.NAME_KEY, name);
        submitIntent.putExtra(MainActivity.EMAIL_KEY, email);
        submitIntent.putExtra(MainActivity.PHONE_KEY, phone);
        submitIntent.putExtra(MainActivity.PHONE_TYPE_KEY, phoneType);

        startActivityForResult(submitIntent, MainActivity.REQUEST_CODE);
    }

    /**
     * Clear all of the inputs in the Main Activity
     */
    protected void clear_all_action() {
        this.nameEditText.setText("");
        this.emailEditText.setText("");
        this.phoneEditText.setText("");
        this.phoneSpinner.setSelection(0);
    }

    /**
     * Exit out of the app gracefully
     */
    protected void exit_action() {
        finish();
    }
}
