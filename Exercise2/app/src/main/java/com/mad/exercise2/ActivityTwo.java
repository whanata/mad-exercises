package com.mad.exercise2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

/**
 * ActivityTwo.java - A simple class to show the result of user information from MainActivity.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class ActivityTwo extends AppCompatActivity implements View.OnClickListener {
    private TextView nameTextView;
    private TextView emailTextView;
    private TextView phoneTextView;
    private TextView phoneTypeTextView;
    private CheckBox agreeCheckbox;
    private Button submitButton;

    /**
     * On Create Function
     * Basically use to instantiate my variables
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        this.nameTextView = this.getTextView(R.id.activity_two_name_textview);
        this.emailTextView = this.getTextView(R.id.activity_two_email_textview);
        this.phoneTextView = this.getTextView(R.id.activity_two_phone_textview);
        this.phoneTypeTextView = this.getTextView(R.id.activity_two_phone_type_textview);
        this.agreeCheckbox = (CheckBox)findViewById(R.id.activity_two_agree_checkbox);
        this.submitButton = this.getButton(R.id.activity_two_submit_button);

        this.showUserInfo();
        this.submitButton.setOnClickListener(this);
    }

    /**
     * Get Button from layout
     * @param id ID of Button
     * @return Button Object with that specified ID
     */
    protected Button getButton(int id) {
        return (Button)findViewById(id);
    }

    /**
     * Get TextView from layout
     * @param id ID of TextView
     * @return TextView Object with that specified ID
     */
    protected TextView getTextView(int id) {
        return (TextView)findViewById(id);
    }

    /**
     * Used to get User Information from the intent in MainActivity Class
     * Display the User Information in the screen
     */
    protected void showUserInfo() {
        Intent getUserInfoIntent = getIntent();

        String name = getUserInfoIntent.getStringExtra(MainActivity.NAME_KEY);
        String email = getUserInfoIntent.getStringExtra(MainActivity.EMAIL_KEY);
        String phone = getUserInfoIntent.getStringExtra(MainActivity.PHONE_KEY);
        String phoneType = getUserInfoIntent.getStringExtra(MainActivity.PHONE_TYPE_KEY);

        this.nameTextView.setText(name);
        this.emailTextView.setText(email);
        this.phoneTextView.setText(phone);
        this.phoneTypeTextView.setText(phoneType);
    }

    /**
     * Send result back to MainActivity - if it has been agreed or not
     */
    protected void sendResultBack() {
        Intent result = new Intent();
        boolean agreed = this.agreeCheckbox.isChecked();

        if (agreed) {
            result.putExtra(MainActivity.AGREE_KEY, getString(R.string.i_agree));
        } else {
            result.putExtra(MainActivity.AGREE_KEY, getString(R.string.i_disagree));
        }

        setResult(Activity.RESULT_OK, result);
        finish();
    }

    /**
     * Does something when a widget has been clicked
     * @param view Current View
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_two_submit_button:
                this.sendResultBack();
                break;
            default:
                break;
        }
    }
}
