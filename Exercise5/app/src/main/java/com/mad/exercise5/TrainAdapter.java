package com.mad.exercise5;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * The RecyclerView Adapter class for the class Train
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class TrainAdapter extends RecyclerView.Adapter<TrainAdapter.ViewHolder> {
    public static final String LOG_TAG = "TrainAdapter";

    private Context mContext;
    private ArrayList<Train> mTrains;

    /**
     * Constructor of the TrainAdapter. Initialize the member variables.
     * @param context
     * @param trains
     */
    public TrainAdapter(Context context, ArrayList<Train> trains) {
        this.mContext = context;
        this.mTrains = trains;
    }

    /**
     * Inner class which represents a single object (One row in a list) in the TrainAdapter.
     * In this case, it will hold one train object.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout mArrivalLinearLayout;
        private ProgressBar mArrivalProgressBar;
        private TextView mArrivalTimeTextView;
        private TextView mArrivalTimeTypeTextView;
        private TextView mPlatformTextView;
        private TextView mStatusTextView;
        private TextView mDestinationTimeTextView;
        private TextView mDestinationTextView;

        /**
         * The constructor for this class.
         * Gets all the views (widgets and layout) for this each row of in the TrainAdapter.
         * It also initialises the ClickListener.
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);
            this.mArrivalLinearLayout =
                    itemView.findViewById(R.id.train_item_arrival_LinearLayout);
            this.mArrivalProgressBar =
                    itemView.findViewById(R.id.train_item_arrival_ProgressBar);
            this.mArrivalTimeTextView =
                    itemView.findViewById(R.id.train_item_arrival_time_TextView);
            this.mArrivalTimeTypeTextView =
                    itemView.findViewById(R.id.train_item_arrival_time_type_TextView);
            this.mPlatformTextView =
                    itemView.findViewById(R.id.train_item_station_platform_TextView);
            this.mStatusTextView =
                    itemView.findViewById(R.id.train_item_status_TextView);
            this.mDestinationTimeTextView =
                    itemView.findViewById(R.id.train_item_destination_time_TextView);
            this.mDestinationTextView =
                    itemView.findViewById(R.id.train_item_destination_TextView);

            this.addOnClickListener();
        }

        /**
         * Set the train information for a single ViewHolder.
         * This sets what the information a single ViewHolder will hold.
         * @param train
         */
        public void setTrainInfo(Train train) {
            this.mArrivalTimeTextView.setText(Integer.toString(train.getArrivalTime​()));
            this.mPlatformTextView.setText(train.getPlatform​());
            this.mStatusTextView.setText(train.getStatus());
            this.mDestinationTimeTextView.setText(train.getDestinationTime​());
            this.mDestinationTextView.setText(train.getDestination​());

            if (train.isLate()) {
                this.mStatusTextView.setTextColor(ContextCompat.getColor(mContext, R.color.train_late));
            } else {
                this.mStatusTextView.setTextColor(ContextCompat.getColor(mContext, R.color.train_on_time));
            }
        }

        /**
         * Private Inner Class for refreshing the arrival time for a single ViewHolder (one train)
         */
        private class refreshTrainTime extends AsyncTask<Void, Void, Integer> {

            /**
             * The function that will run in another thread, separate from main thread.
             * Get random integer between 1 and 20 inclusively and return that integer.
             * This will be used as the new refreshed arrival time of the train.
             * Also sleep for 2 seconds after getting the new arrival time.
             * @param voids This will have nothing.
             * @return The refreshed Arrival Time of train
             */
            @Override
            protected Integer doInBackground(Void... voids) {
                int arrivalTime = Helper.getRandomInt(1, 20);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    Log.d(TrainAdapter.LOG_TAG, "Interrupt Sleeping", e);
                }

                return arrivalTime;
            }

            /**
             * Make ProgressBar (Spinner) match the LinearLayout of the arrival section.
             * Also make ProgressBar visible and make arrival information invisible.
             */
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                // Match ProgressBar (Spinner) with the LinearLayout that contains it
                mArrivalLinearLayout.getLayoutParams().width = mArrivalLinearLayout.getWidth();
                mArrivalLinearLayout.getLayoutParams().height = mArrivalLinearLayout.getHeight();
                mArrivalTimeTextView.setVisibility(View.GONE);
                mArrivalTimeTypeTextView.setVisibility(View.GONE);
                mArrivalProgressBar.setVisibility(View.VISIBLE);
            }

            /**
             * Set new arrival time for this specific train.
             * Remove ProgressBar and replace it back with the arrival information.
             * @param arrivalTime The new arrival time for a train.
             */
            @Override
            protected void onPostExecute(Integer arrivalTime) {
                super.onPostExecute(arrivalTime);
                Train train = mTrains.get(getAdapterPosition());
                train.setArrivalTime​(arrivalTime);
                TrainAdapter.this.notifyItemChanged(getAdapterPosition());
                mArrivalProgressBar.setVisibility(View.GONE);
                mArrivalTimeTextView.setVisibility(View.VISIBLE);
                mArrivalTimeTypeTextView.setVisibility(View.VISIBLE);
            }
        }

        /**
         * Add the onClickListener for each row in the RecyclerView of the trains.
         */
        protected void addOnClickListener() {
            View.OnClickListener listener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    switch(view.getId()) {
                        case R.id.train_item_arrival_LinearLayout:
                            refreshAction();
                            break;
                        default:
                            break;
                    }
                }
            };

            this.mArrivalLinearLayout.setOnClickListener(listener);
        }

        /**
         * Refresh a single train row in the RecyclerView (Add new arrival time).
         */
        protected void refreshAction() {
            new refreshTrainTime().execute();
        }
    }

    /**
     * Create a single ViewHolder.
     * @param parent
     * @param viewType
     * @return new ViewHolder Object
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.train_item, parent, false);

        return new ViewHolder(itemView);
    }

    /**
     * Bind ViewHolder to a Train Object
     * @param holder The ViewHolder Object.
     * @param position Position of the ViewHolder in context of the RecylerView
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Train train = this.mTrains.get(position);
        holder.setTrainInfo(train);
    }

    /**
     * Count of the rows in the RecyclerView.
     * @return Count of rows in the RecyclerView.
     */
    @Override
    public int getItemCount() {
        return this.mTrains.size();
    }
}
