package com.mad.exercise5;

import java.util.Random;

/**
 * A helper class which stores functions that don't necessarily fit in the other class.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class Helper {

    /**
     * Get a random int from a range between `min` and `max` inclusively.
     * @param min The lowest number the random integer can be.
     * @param max The highest number the random integer can be.
     * @return A random integer between `min` and `max` inclusively.
     */
    public static int getRandomInt(int min, int max) {
        Random random = new Random();
        return random.nextInt((max - min) + 1) + min;
    }
}
