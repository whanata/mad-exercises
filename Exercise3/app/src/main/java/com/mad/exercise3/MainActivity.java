package com.mad.exercise3;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * MainActivity.java - The main class that outlines the MainActivity.
 * Includes input user information, rotation of screen and swapping of picture.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity {
    public static final String FIRST_PIC_KEY = "FIRST_IMAGE";
    public static final String FIRST_NAME_KEY = "FIRST_NAME";
    public static final String LAST_NAME_KEY = "LAST_NAME";
    public static final String PHONE_KEY = "PHONE";
    public static final String EMAIL_KEY = "EMAIL";

    public static final String LOG_TAG = "MAD";

    private ImageView mTopPicImageView;
    private EditText mFirstNameEditText;
    private EditText mLastNameEditText;
    private EditText mPhoneEditText;
    private EditText mEmailEditText;
    private Button mSwapButton;
    private Button mRotateButton;

    private boolean mFirstImage;

    /**
     * Instantiate my variables - called when app is first open.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(MainActivity.LOG_TAG, "onCreate()");

        this.mTopPicImageView = this.getImageView(R.id.activity_top_pic_imageview);
        this.mFirstNameEditText = this.getEditText(R.id.activity_main_first_name_edittext);
        this.mLastNameEditText = this.getEditText(R.id.activity_main_last_name_edittext);
        this.mPhoneEditText = this.getEditText(R.id.activity_main_phone_edittext);
        this.mEmailEditText = this.getEditText(R.id.activity_main_email_edittext);
        this.mSwapButton = this.getButton(R.id.activity_main_swap_button);
        this.mRotateButton = this.getButton(R.id.activity_main_rotate_button);

        this.mFirstImage = true;

        this.mSwapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                swapAction();
            }
        });

        this.addOnFocusChangeListener();
    }

    /**
     * Save current input information and picture.
     * Called when app is paused (app is killed or pushed to background).
     * @param outState Stored information
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(MainActivity.LOG_TAG, "onSaveInstanceState()");

        String firstImageText = Boolean.toString(this.mFirstImage);
        String firstName = this.mFirstNameEditText.getText().toString();
        String lastName = this.mLastNameEditText.getText().toString();
        String phone = this.mPhoneEditText.getText().toString();
        String email = this.mEmailEditText.getText().toString();

        outState.putString(MainActivity.FIRST_PIC_KEY, firstImageText);
        outState.putString(MainActivity.FIRST_NAME_KEY, firstName);
        outState.putString(MainActivity.LAST_NAME_KEY, lastName);
        outState.putString(MainActivity.PHONE_KEY, phone);
        outState.putString(MainActivity.EMAIL_KEY, email);
    }

    /**
     * Load information saved from onSaveInstanceState().
     * Called when app starts again from pause.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(MainActivity.LOG_TAG, "onRestoreInstanceState()");

        String firstImageText = savedInstanceState.getString(MainActivity.FIRST_PIC_KEY);
        String firstName = savedInstanceState.getString(MainActivity.FIRST_NAME_KEY);
        String lastName = savedInstanceState.getString(MainActivity.LAST_NAME_KEY);
        String phone = savedInstanceState.getString(MainActivity.PHONE_KEY);
        String email = savedInstanceState.getString(MainActivity.EMAIL_KEY);

        this.mFirstImage = Boolean.parseBoolean(firstImageText);
        if (this.mFirstImage) {
            this.mTopPicImageView.setImageResource(R.drawable.rick);
        } else {
            this.mTopPicImageView.setImageResource(R.drawable.morty);
        }

        mFirstNameEditText.setText(firstName);
        mLastNameEditText.setText(lastName);
        mPhoneEditText.setText(phone);
        mEmailEditText.setText(email);
    }

    /**
     * Add onFocusChangeListener to a widget.
     * Called when a widget is either in or out of focus.
     */
    protected void addOnFocusChangeListener() {
        View.OnFocusChangeListener listener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                switch(view.getId()) {
                    case R.id.activity_main_email_edittext:
                        focusEmailAction(hasFocus);
                        break;
                    default:
                        break;
                }
            }
        };

        this.mEmailEditText.setOnFocusChangeListener(listener);
    }

    /**
     * Shows a toast (When email EditText is pressed)
     * @param hasFocus Current focus state
     */
    protected void focusEmailAction(boolean hasFocus) {
        if (hasFocus) {
            Toast.makeText(this, getString(R.string.email_focus), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, getString(R.string.no_email_focus), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Swap pictures at the top of the app.
     * Also will remember the current picture.
     * The action when Swap Button is pressed.
     */
    protected void swapAction() {
        if (this.mFirstImage) {
            this.mTopPicImageView.setImageResource(R.drawable.morty);
            this.mFirstImage = false;
        } else {
            this.mTopPicImageView.setImageResource(R.drawable.rick);
            this.mFirstImage = true;
        }
    }

    /**
     * Change orientation of the screen (portrait or landscape).
     * Call this when Rotate Button is pressed.
     * @param view Current view of activity
     */
    public void rotateAction(View view) {
        int currentOrientation = getResources().getConfiguration().orientation;
        switch(currentOrientation) {
            case Configuration.ORIENTATION_PORTRAIT:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                break;
        }
    }

    /**
     * Get EditText Widget.
     * @param id ID of EditText Widget
     * @return EditText
     */
    protected EditText getEditText(int id) {
        return (EditText)findViewById(id);
    }

    /**
     * Get Button Widget.
     * @param id ID of Button Widget
     * @return Button
     */
    protected Button getButton(int id) {
        return (Button)findViewById(id);
    }

    /**
     * Get ImageView Widget.
     * @param id ID of ImageView Widget
     * @return ImageView
     */
    protected ImageView getImageView(int id) {
        return (ImageView)findViewById(id);
    }
}
